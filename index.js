const axios = require('axios');

const htmlparser2 = require("htmlparser2");

var checkText = false; //check city
var checkDd = false; //check date
var check = false; //check temp
var found = false;
var count = 0;

const parser = new htmlparser2.Parser({
    onopentag(name, attribs) {
        if (name == "h1") {
            checkText = true;
        }
        if (name == "dd") {
            checkDd = true;
        }
        if (name == "dd" && attribs.class == "mrgn-bttm-0 wxo-metric-hide") {
            check = true;
        }
    },
    ontext(text) {
        if (checkText) {
            console.log(text.substring(0, text.indexOf(',')));
            checkText = false;
        }
        if (checkDd && (text.includes('AM') || text.includes('PM')) && !found) {
            console.log(text);
            found = true;
        }
        if (check) {
            count++;
            if (count == 3)
                console.log(text);
        }
    },
    onclosetag(name) {
        checkDd = false;
        check = false;
    }
}, { decodeEntities: true });

// Make a request for a user with a given ID
axios.get('https://weather.gc.ca/city/pages/nl-24_metric_e.html')
    .then(result => {
        parser.write(
            result.data
        );
        parser.end();
    })
    .catch(error => { console.log(error) });